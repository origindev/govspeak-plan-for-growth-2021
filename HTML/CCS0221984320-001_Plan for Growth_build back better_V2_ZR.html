##Build Back Better: our plan for growth at a glance

Over the past year, we have faced substantial challenges, and the last few months have been amongst the most difficult yet. Businesses and schools have had to close, families and friends have been kept apart, and tragically, lives have been lost.

The UK Government has put in place an unprecedented economic package, providing businesses and individuals with support and certainty over the course of the pandemic, spending hundreds of billions to support people’s jobs, businesses, and public services across the UK. However, COVID-19 and the restrictions put in place to stop the spread of the virus caused the largest fall in annual GDP in 300 years.

But we are well placed to confront these challenges, rebounding to build back better: the UK’s departure from the European Union presents further opportunities for us to do things differently, opening up new ways to drive growth. The UK is one of the world’s largest economies, open and dynamic, with strong institutions and world-class universities. 

The UK is a great place to start and grow a business, home to some of the world’s best companies. We have an international reputation for science: the development and manufacture of the Oxford/AstraZeneca vaccine demonstrated the strong partnerships that exist between universities and businesses in the UK. This strength extends to sectors such as aerospace, the creative industries, financial services, and emerging industries such as AI and fintech.

The last few decades have seen increased prosperity in London and the South East, but without commensurate improvements in the rest of the UK. The primary objective of this government is to change that, ensuring no region is left behind as we achieve greater economic prosperity. Our cities will be the engines for this growth, and our long-term vision is for every region and nation to have at least one internationally competitive city, driving the prosperity of the surrounding region and propelling forward the national economy. Our towns are crucial too - we will ensure that they are places that people are proud to live and raise their families, with good schools, vibrant high streets, and access to jobs that give everyone a fair chance to achieve their full potential.

Our plan to build back better takes a transformational approach, tackling long-term problems to deliver growth that creates high-quality jobs across the UK and makes the most of the strengths of the Union. We must retain our guiding focus on achieving the people’s priorities: levelling up the whole of the UK, supporting our transition to net zero, and supporting our vision for Global Britain.

####We will do this by building on three core pillars of growth:

<table class="financial-data source-tableeditor">
<thead>
<tr>
<td></td>
<th>We will...</th>
</tr>
</thead>
<tbody>
<tr>
<th><strong>Infrastructure</strong></th>
<td>Stimulate short-term economic activity and drive long-term productivity improvements via record investment in broadband, roads, rail and cities, as part of our capital spending plans worth &pound;100 billion next year.<br /><br />Connect people to opportunity via the UK-wide Levelling Up Fund and UK Shared Prosperity Fund, as well as the Towns Fund and High Street Fund, to invest in local areas.<br /><br />Help achieve net zero via &pound;12 billion of funding for projects through the Ten Point Plan for a Green Industrial Revolution.<br /><br />Support investment through the new UK Infrastructure Bank which will &lsquo;crowd-in&rsquo; private investment to accelerate our progress to net zero, helping to level up the UK. This will invest in local authority and private sector infrastructure projects, as well as providing an advisory function to help with the development and delivery of projects.</td>
</tr>
<tr>
<th><strong>Skills</strong></th>
<td>Support productivity growth through high-quality skills and training: transforming Further Education through additional investment and reforming technical education to align the post-16 technical education system with employer demand.<br /><br />Introduce the Lifetime Skills Guarantee to enable lifelong learning through free fully funded Level 3 courses, rolling out employer-led skills bootcamps, and introducing the Lifelong Loan Entitlement.<br /><br />Continue to focus on the quality of apprenticeships and take steps to improve the apprenticeship system for employers, through enabling the transfer of unspent levy funds and allowing employers to front load apprenticeship training.</td>
</tr>
<tr>
<th><strong>Innovation</strong></th>
<td>Support and incentivise the development of the creative ideas and technologies that will shape the UK&rsquo;s future high-growth, sustainable and secure economy.<br /><br />Support access to finance to help unleash innovation, including through reforms to address disincentives for pension funds to invest in high-growth companies, continued government support for start ups and scale ups through programmes such as British Patient Capital, and a new &pound;375 million Future Fund: Breakthrough product to address the scale up gap for our most innovative businesses.<br /><br />Develop the regulatory system in a way that supports innovation.<br /><br />Attract the brightest and best people, boosting growth and driving the international competitiveness of the UK&rsquo;s high-growth, innovative businesses.<br /><br />Support our small and medium-sized enterprises (SMEs) to grow through two new schemes to boost productivity: Help to Grow: Management, a new management training offer, and Help to Grow: Digital, a new scheme to help 100,000 SMEs save time and money by adopting productivity-enhancing software, transforming the way they do business.</td>
</tr>
</tbody>
</table>

####Not all growth is created equal. The growth we drive will:

<table class="financial-data source-tableeditor">
<tbody>
<tr>
<th><strong>Level up the whole of the UK</strong></th>
<td>Regenerate struggling towns in all parts of the UK viathe UK Shared Prosperity Fund and the UK-wide Levelling Up Fund.<br /><br />Realise our long-term vision for every region and nation to have at least one globally competitive city at its heart to help drive prosperity. This includes City and Growth Deals, &pound;4.2 billion in intra-city transport settlements from 2022-23, and continued Transforming Cities Fund investment to 2022-23.<br /><br />Catalyse centres of excellence, supporting individuals across the country to access jobs and opportunities by ensuring digital and transport connectivity, by establishing a new UK Infrastructure Bank in the North of England and by relocating 22,000 Civil Service roles out of London.<br /><br />Strengthen the Union, creating Freeports across the country &ndash; including in Scotland, Wales and Northern Ireland &ndash; and delivering the Union Connectivity Review, reviewing options to improve our sea, air and land-links across the four nations.</td>
</tr>
<tr>
<th><strong>Support the transition to Net Zero</strong></th>
<td>Invest in net zero to create new opportunities for economic growth and jobs across the country, including supporting up to 60,000 jobs in the offshore wind sector, 50,000 jobs in carbon capture, usage and storage (CCUS) and up to 8,000 in hydrogen in our industrial clusters.<br /><br />Grow our current net zero industries and encourage new ones to emerge. This includes working with industry, aiming to generate 5GW of low carbon hydrogen production capacity and capture 10Mt CO2/year using CCUS by 2030, and ending the sale of new petrol and diesel cars and vans in 2030.</td>
</tr>
<tr>
<th><strong>Support our vision for Global Britain</strong></th>
<td>Cooperate with partners to inspire and shape international action on our domestic priorities, including through our G7 Presidency and COP26. <br /><br />Role-model openness to free and fair trade, working internationally to strengthen the multilateral system and the World Trade Organization and using preferential agreements and bilateral trade relationships to directly expand trading opportunities for UK businesses. <br /><br />Develop a new export strategy to align our support for exporters with our plan for growth and sectoral priorities, opening UK Government trade hubs in Scotland, Wales and Northern Ireland and increasing UK Export Finance lending capacity.</td>
</tr>
</tbody>
</table>

We will carry this out with a relentless focus on delivery, using clear metrics to monitor progress and ensure success. Over the next twelve months, we will build on the strong foundation set out in this plan to make real progress.

By combining new approaches with our well-established strengths, we will deliver growth that benefits the whole of the United Kingdom, creates quality jobs, and ensures that we build back better.
