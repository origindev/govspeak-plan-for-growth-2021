##Levelling up

Place matters. Many people are rooted to their local area because of its civic identity and their social and family connections: over 40% of workers have only ever worked in the same local area as they were born<a href="#endnote-001">1</a>. But there are parts of the country where people feel left-behind, that they are not getting fair access to jobs, wages and skills opportunities, and that their local priorities are not being delivered on by government. 

Levelling up is about improving everyday life for people in those places. It is about ensuring people can be proud of their local community, rather than feeling as though they need to leave it in order to reach their potential.

This is why the UK Government’s most important mission is to unite and level up the country.  We want to improve everyday life for communities throughout the UK.  Where people live should not be a barrier to their life chances.  

####Weekly wages across UK nations and regions

!!1

We will tackle geographical disparities in key services and outcomes across the UK: improving health, education, skills, increasing jobs and growth, building stronger and safer communities and improving infrastructure and connectivity. We will focus on boosting regional productivity where it is lagging to improve job opportunities and wages.  

Our city regions are critical for driving economic growth and long-term prosperity. We want to achieve dynamic regional economies with high-value centres of excellence. The success of these are vital to the success of the wider region, including increasing the opportunities available for the towns and communities surrounding these cities.

Some communities and towns are struggling, from ex-industrial areas to coastal communities. By improving access to services and the outcomes that matter most for life-chances, they will see social, economic and cultural regeneration. 

Above all, this is a plan that will build on the strengths of the Union.  The Union is core to our economic model and at the heart of our prosperity. We must therefore ensure the whole of the UK is firing on all cylinders. This means delivering on the infrastructure plans the government has set out for places across the UK, including through funding provided to the devolved administrations, and taking forward the Union Connectivity Review.  

We are working closely with local authorities in every corner of the UK through the UK Shared Prosperity Fund, the Community Renewal Fund, the Community Ownership Fund, and the Levelling Up Fund to strengthen the connection between central government and citizens.

Fundamentally, we are pursuing a new economic approach. We will: 

- unleash the potential of our great cities, ensuring they drive growth in every region and nation in the UK; 

- invest in public services – the NHS, schools and colleges – to make them high quality everywhere in the country;

- invest in towns across the UK, connecting people and places to high quality jobs, driving private sector investment, and creating places where people feel secure and feel proud to live

All the themes in our plan for growth play a role in supporting our overarching mission to level up and unite the country.  

###Supporting individuals across the country to reach their potential

The most important pillar in our approach to levelling up is supporting individuals across the UK to reach their potential, investing in people in every place and region.  

Differences in levels of ‘human capital’ between places and regions, i.e. education, skills, health, are an important explainer of differences in regional outcomes. Research finds that up to 90% of area-level disparities in wages can be explained by the distribution of high-skilled workers<a href="#endnote-002">2</a>. 50% of the population in London have graduate-level qualifications, compared to 33% of the population in the North East of England. But more than this, people’s experience with health and education determine life-chances, wellbeing and social connection. In some places outcomes are poor, and this undermines belief that government can deliver for a community. We will strengthen these vital public services to address disparities in life-chances driven by key public services like health, education and skills. Some of the main themes of this investment are set out throughout this document.

####Regenerating struggling towns

People across the UK have lost out from structural economic changes simply because of where they live.  Our approach to tackling disadvantage in these struggling towns focuses on a three-pronged approach to support the regeneration of struggling towns, underpinned by world-class public services to drive improved outcomes to improve life-chances and deliver on our vision for every town across the UK to be safe, healthy and prosperous.

First, the UK Government is focusing on supporting individuals in struggling towns to have the best possible life-chances by improving outcomes in education, skills, health, and participation in the workforce. Public services in these places often face more challenges than elsewhere and addressing these requires additional intervention to ensure they can best serve the needs of the community. We will use the UK Shared Prosperity Fund to target particular challenges facing individuals and communities in struggling towns across the UK, in collaboration with local authorities, and in a way that empowers local areas and strengthens local public services.

Second, we are delivering a step-change in how we support struggling towns by working in partnership with local institutions to invest in local priorities to support the local economy and improve quality of life, showing that government can deliver on local priorities such as improving local infrastructure, driving up the supply of housing where it is needed, and crime reduction. The £4.8 billion UK-wide Levelling Up Fund, will enable places to plan for their future. The Levelling Up Fund consolidates local growth funding streams, reducing the burden on places and breaking down siloes between UK Government departments. The prospectus for the Levelling Up Fund published alongside the Budget sets out how local areas across the UK can apply for funding to invest in local infrastructure priorities, with bids prioritised from places most in need of investment. Through the Towns Fund, we are also supporting the regeneration of towns across England. At Budget 2021, we are announcing 44 new Town Deals with towns across the country, investing in local priorities.

####Towns Fund Case Study: Southport

The Government is investing £37.5 million in Southport through the Towns Fund, supporting Sefton Council and the Southport Town Deal Board to breathe new life into the town. 

Southport’s economy is characterised by a high proportion of part-time jobs in the retail, leisure and visitor economy which have been exposed to economic shocks both prior to and related to COVID-19.  A lack of all-weather, all year-round attractions has resulted in seasonality and a short summer season adding to the vulnerability in the sector. This is compounded by young people leaving the town as a result of the lack of opportunities.

With this funding from Government, Southport plans to create: 

- up to 30,000 sqm of new and accessible public space, with priority for walking and cycling, to improve the connections between the waterfront and the town centre; 

- a new world-class events centre, light fantastic display and all year-round attraction on the waterfront

This infrastructure will improve the visitor experience on arrival; it will also help to create a year-round, all-weather economy that is not only more diverse but also provides opportunities for young people to see their future in the town and choose to stay. 

Third, we are also supporting private sector investment and new jobs in these communities: we are powering up local economies to ensure everyone can share in the prosperity of the UK. For example, the UK Government is backing the development of hydrogen hubs in the Tees Valley and Holyhead. This will build up the UK’s capabilities in the green industries of the future and support the growth of green industrial clusters across coastal and post-industrial communities. 

###Helping city regions become globally competitive

Cities are a fundamental driver of productivity growth. They play a critical role in the success of the wider region – successful regions benefit from strong cities to anchor growth. Our long-term vision is therefore for every region and nation of the UK to have at least one globally competitive city at its heart, helping to drive prosperity and increasing opportunity for all those who live nearby. 

But our regional cities underperform: 

####%Difference in GDP per worker to national average

!!1

Successive governments have sought to address long-standing regional disparities. We will back cities as engines of the UK economy, focusing government action on addressing the barriers to growth. We will build strong, enduring partnerships with local institutions, such as universities, businesses and city leaders to deliver our shared vision of globally competitive regional cities, pulling up prosperity in the wider region.   

To achieve this vision, our core cities like Birmingham, Manchester and Glasgow must become well-connected, innovative hubs of high-value activity. Government intervention can break down the barriers to this, ensuring strong local and regional transport links, planning policies fit for the future, excellent universities, and a business environment that supports dynamism and inward investment. We will bring to bear the full range of levers available to government, across skills, infrastructure, innovation, the business environment and finance to deliver this vision. 

We are already investing to address the barriers to productivity that our core cities face. Budget 2020 committed to invest £4.2 billion in intra-city transport settlements from 2022-23, through five-year consolidated funding settlements for eight city regions, including Greater Manchester, Liverpool City Region, West Midlands, West Yorkshire, Sheffield City Region, West of England and Tees Valley, subject to the creation of appropriate governance arrangements to agree and deliver funding. Budget 2021 takes the first step toward delivering the government’s commitment - confirming capacity funding in 2021-22 to support those city regions with appropriate governance arrangements already in place to begin preparations for settlements, enabling them to develop integrated investment-ready transport plans that will deliver on local priorities such as tackling congestion and driving productivity. 

We are investing in the National Home Building Fund (NHBF), with initial funding of £7.1 billion over the next four years to unlock up to 860,000 homes, including delivery of the Brownfield Fund, announced at Budget 2020 for Mayoral Combined Authorities like Greater Manchester and Liverpool City Region and an additional £100 million for non-Mayoral Combined Authorities in 2021-22. Almost £2.9 billion will be invested in City and Growth Deals to drive forward local economic priorities in Scotland, Wales and Northern Ireland.

####Case Studies: City and Growth Deals

- Scotland: The UK Government, working in collaboration with the Scottish Government, is investing £103 million in the Ayrshire Growth Deal to deliver eight projects. One of them is the transformation of the former Johnnie Walker bottling site into HALO, a dynamic commercial, educational and advanced training hub, with a focus on sustainability and low carbon energy and which will create up to 200 cybersecurity job placement opportunities for young people. 

- Wales: The UK Government, working in collaboration with the Welsh Government, is investing £115.6 million in the the Swansea Bay City Deal which aims to transform the regional economy by building its skills base. Projects include Yr Egin (Creative Digital Cluster), a purpose-built facility with auditorium, professional post-production facilities and spaces for networking and community activities, which has already created 100 jobs and attracted 17 private sector companies to the site. 

- Northern Ireland: The UK Government, working in collaboration with the Northern Ireland Executive, is investing £350 million in the Belfast Region City Deal, to encourage innovation and transform digital capabilities within the region. Projects include the Global Innovation Institute and the Regional Innovators Network, creating a digital ecosystem for researchers and businesses to develop and apply new technology to improve productivity. 

###Catalysing centres of excellence and helping people connect to opportunity

We are investing across digital and physical connectivity to support growth from corner to corner of the UK and ensure people do not have to relocate to succeed where they live. Infrastructure investment can bring individuals closer to opportunity: connectivity supports flows of trade, information and people from corner to corner of the UK, spreading opportunity and prosperity across regions and nations. 

HS2 is this government’s flagship national transport project, delivering essential North-South connectivity between some of the UK’s biggest cities. The Integrated Rail Plan for the Midlands and the North, will set out how best to integrate HS2 Phase 2b with wider transport plans across the North and Midlands, ensuring all parts of the country benefit from opportunities for prosperity. We are delivering the largest ever investment in motorways and major A-roads worth over £27 billion between 2020 and 2025, as well as investing £1.7 billion in our local roads in the next 12 months. The Union Connectivity Review is reviewing options to improve our sea, air and land-links across the four nations. 

The UK Government’s £5 billion UK Gigabit Programme will subsidise the rollout of gigabit-capable broadband to ensure no area will be left behind. In addition, the government is investing £500 million, matched by industry, to deliver high-quality 4G mobile coverage from at least one operator across 95% of the UK by 2025, through the Shared Rural Network. This will have major benefits in rural areas, and for Scotland, Wales and Northern Ireland.

####Changing the way we invest in places

Delivering growth for the whole of the UK over the long-term means doing things differently. We recognise that the existing local institutional framework is complex and fragmented. Working in tandem with local communities and businesses, empowered  local institutions will help to drive forward change in their areas, setting strategic direction and delivering for places. Local growth policies must be coherent and coordinated to maximise impact.

This is the context in which we are fundamentally re-wiring the way the UK Government delivers for places to ensure it better reflects the communities it serves. We have refreshed the Green Book to improve the understanding of the impacts of policies on places. We are relocating 22,000 Civil Service roles out of London and the South East by 2030 through the Places for Growth programme, bringing policy-makers closer to the communities they serve. HM Treasury will establish a new economic campus in the North of England, helping to shift policymaking away from Whitehall. We are relocating Civil Service jobs into Scotland, including to Queen Elizabeth House, the newly opened UK Edinburgh Hub. We are creating a new UK Infrastructure Bank in the North of England, and operating across the UK, to catalyse private investment in infrastructure projects.

Eight metro mayors have been elected across the country, with a ninth, for West Yorkshire, planned for 2021. We want to devolve and decentralise to give more power to local communities, providing opportunity across the country. Through devolution deals in England, the UK Government has committed £7.5 billion of unringfenced ‘gainshare’ investment over 30 years for the nine Mayoral Combined Authorities, to be spent on local priorities. We will bring forward a Devolution & Local Recovery White Paper to set out expanded devolution arrangements, building on the success of the directly-elected Mayors. 

####Making sure our international strategy works for the whole UK

Openness to trade has allowed the UK to participate in and access specialised global supply chains delivering innovative, high-quality products such as smartphones and MRI scanners at more affordable prices. While UK consumers have benefited, this posed a particular challenge to areas with a substantial industrial base, newly exposed to international competition. We have an opportunity to shape our newly independent trade and investment policy to ensure it meets our domestic economic priorities, and that all regions can benefit. 

This means:

- acting decisively to tackle unfair competition and make sure our businesses are able to compete, where necessary taking action to protect UK businesses;

- using new initiatives such as Freeports to level up communities across the UK, support jobs, and turbo-charge our economic recovery; 

- targeting our support for inward investment to unlock growth across the whole UK, including through the new Office for Investment

####Our plan for levelling up

<table class="financial-data source-tableeditor">
<thead>
<tr>
<th>Why levelling up is important</th>
<th>What this means</th>
<th>What we&rsquo;re doing</th>
</tr>
</thead>
<tbody>
<tr>
<td>Where people live shouldn&rsquo;t be a barrier to their life chances: everyone should get a fair chance to get on in life, wherever they live.</td>
<td>Economic, social, and cultural regeneration of struggling towns.</td>
<td>Investing in local priorities across the UK to increase opportunity in struggling communities, through the UK Shared Prosperity Fund, the Community Ownership Fund, the Community Renewal Fund, the Towns Fund.</td>
</tr>
<tr>
<td>We need to address regional economic disparities.</td>
<td>Boosting growth in our regional cities to deliver our long-term vision to have a globally competitive city in every region and nation of the UK and drive up prosperity.</td>
<td>Accelerating funding to City and Growth Deals in Scotland and Wales.<br /><br />&pound;4.2 billion in intra-city transport settlements from 22-23; continued Transforming Cities Fund Investment to 2022-23.<br /><br />Investing in housing via the National Home Building Fund and Brownfield housing funding.<br /><br />Delivering Freeports across the country - including in Scotland, Wales and Northern Ireland.</td>
</tr>
<tr>
<td>We need to ensure people can access opportunities.</td>
<td>Backing regional centres of industrial excellence.<br /><br />Connecting people to opportunity.</td>
<td>Backing HS2, the Union Connectivity Review, and the upcoming Integrated Rail Plan for the Midlands and the North.<br /><br />Making the largest ever investment in motorways and A-roads worth over &pound;27 billion.<br /><br />Completing the &pound;5 billion UK Gigabit Programme and the Shared Rural Network.<br /><br />Establishing eight Freeports in England plus at least one in each of Scotland, Wales and Northern Ireland.</td>
</tr>
<tr>
<td>We need to embed lasting change in outcomes across the UK.</td>
<td>Changing the way we deliver for places.</td>
<td>Relocating 22,000 Civil Servants outside of London and the South East by 2030.<br /><br />New UK Infrastructure Bank.<br /><br />Bringing forward the Devolution and Local Recovery White Paper.</td>
</tr>
</tbody>
</table>



