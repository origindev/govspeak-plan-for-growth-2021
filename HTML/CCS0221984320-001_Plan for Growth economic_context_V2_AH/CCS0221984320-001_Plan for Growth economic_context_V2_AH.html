
##Economic context

The challenges faced by the UK over the past twelve months have been substantial, and the last few months have been amongst the most difficult yet. Businesses and schools have had to close, families and friends have been kept apart, and tragically, lives have been lost.

COVID-19 and the restrictions put in place to stop the spread of the virus caused the largest fall in annual GDP in 300 years. The UK Government has put in place significant support measures to limit the rise in unemployment and insolvencies. This includes the furlough scheme, support for the self-employed, government backed loans, and grants for businesses forced to close. This support package, worth hundreds of billions, has helped all regions and nations of the UK.

As the economy starts to recover, we must confront the challenges created by COVID-19 and minimise the risk of lasting economic damage from this crisis across all corners of the UK. Jobs are the key economic priority. There will be a period of adjustment, as some jobs are lost, and people find new work in growing businesses or spot new opportunities to start their own. That is why through the Plan for Jobs the UK Government is focusing on delivering employment support, including work search or support with retraining, to those who need it most, from helping the recently unemployed to swiftly find new work, to offering greater support for people who will find that journey more difficult. 

####Plan for Jobs

The UK has one of the most successful labour markets, which pre-COVID-19 delivered record employment rates. The government has taken unprecedented action to protect jobs during the pandemic, most notably through the Coronavirus Job Retention Scheme (CJRS). However, the government is also supporting people who have unfortunately lost their jobs, helping them search for work or retrain, by:

- Significantly expanding Department for Work and Pensions (DWP) Jobcentre support, including doubling the number of work coaches, additional investment into the Flexible Support Fund to provide direct support at a local level, and additional intensive support to those who have been unemployed for at least three months.

- Introducing the £2 billion Kickstart scheme, which will provide young people at risk of long-term unemployment with fully-subsidised jobs to give them experience and skills. 

- Introducing the £2.9 billion Restart programme, which will provide regular, personalised support for those on Universal Credit who have been searching for work for over a year.

- Supporting people to build the skills they need to get into work, with a substantial expansion of existing provision, providing funding to expand the number of traineeships and sector-based work academy placements, alongside further support for new apprentice hiring, which enables people to work while training.

These policies are important for the short-term recovery, helping to keep people close to the labour market and develop work experience and skills. They will help to ensure that the wider government labour market and skills offer also helps achieve long-term prosperity. 

The scale of the challenge ahead is large. In December 2020 GDP was 6.3% below its February 2020 level; and business investment in the final quarter of last year was 19.2% lower than the year before. At its lowest point in November 2020, the number of employees had fallen by 882,000 (-3%) since February. 

The COVID-19 shock has highlighted the great adaptability and resilience of UK businesses. Employers moved at speed to migrate services online, enabled their employees to work from home where possible, and adapted their premises to be COVID-secure. Over 60% of businesses have adopted new digital technologies during the pandemic[^1]. 

That adaptability and resilience will prove vital in the months and years ahead, as the UK economy continues to transform. Technological innovations will change every sector of the economy, supporting higher earnings and shorter working days. New trading relationships will offer opportunities for our businesses to experience the benefits of exporting. The UK Government, alongside businesses and individuals, will harness the benefits of the transition to net zero, including an enhanced natural environment, cleaner air in our cities and new green jobs. Private sector investment will be a vital complement to planned public sector investment, and so it is crucial that we create the right conditions to unlock that investment and create jobs and growth. 

The 2017 Industrial Strategy set out a cross-economy approach to boost productivity. But much has changed since 2017, so it is right that we create a new framework for how we will build back better. This document details our focus on infrastructure, skills and innovation. It reflects new opportunities available to us following our exit from the European Union, opening up new ways to drive growth and supporting our vision for Global Britain. This plan also demonstrates that we will not pursue growth at the expense of the government&apos;s wider objectives - instead we see real opportunities to boost our economic performance while levelling up across the UK and in a way that contributes to reaching net zero emissions.

####Meeting modern challenges and thriving

The UK is well placed to meet both the challenges posed by COVID-19 disruption and to grasp the opportunities from our exit from the European Union, and from the social and economic adjustments due in the coming decades.

- An open and dynamic economy – the UK has a flexible labour market that has generally seen lower unemployment than many of its European competitors over the last few decades. The UK has historically attracted international investment, with the largest stock of inward Foreign Direct Investment in Europe[^2]. In 2019 our trade to GDP ratio was higher than the world average[^3]. Our openness to trade and investment underpins our prosperity and drives opportunity across the UK. 

- World class knowledge and research – The UK has a high share of the adult population with tertiary education, over seven percentage points higher than the OECD average[^4]. Four of the world’s top 20 universities are in the UK[^5]. The UK punches above its weight on article downloads, citations and world’s most highly-cited articles. The UK is ranked 4th on the 2020 Global Innovation Index[^6]. 

- A stable framework for growth and strong institutions – The UK’s macroeconomic framework and institutions, for example through the commitment to low, stable inflation and financial stability through an independent central bank, enable the right conditions for the economy to succeed. Macroeconomic stability gives consumers, businesses and financial markets confidence to participate and invest in the real economy. The UK is committed to independent economic regulation and its institutions, such as its legal system, are respected around the world.

- Leading innovative and high growth sectors – The UK has strengths in a diverse set of industries. An international reputation for science with strengths across sectors, including pharmaceuticals, aerospace, creatives, financial services, and professional and business services; and in emerging industries such as AI and fintech. The UK is home to some of the best businesses in the world and we are leading the way on the path to net zero. 

- A great place to start and grow a business – The UK is one of the best places in the world to start a business, with a stable regulatory environment and one of the highest birth rates of enterprises in the OECD. The UK has relatively deep capital markets that supports firms to grow. The UK has the lowest level of regulatory barriers to firm entry and competition in the OECD[^7] and is ranked 8th on the 2020 World Bank Ease of Doing Business Index[^8]. 

###Creating an environment that enables the UK to succeed

Economic growth is driven by increasing employment and productivity. The UK must return to growing employment and take action to address weak productivity in order to secure a sustainable increase in growth.

UK businesses have a good record in creating jobs, with the UK outperforming most other comparable OECD countries on employment. At the start of last year, the UK’s employment rate was at a record high (76.6% in the three months to February 2020), and 3.5 percentage points higher than its pre-financial crisis peak. This employment growth was led by the private sector. The plan for growth will help to build a business environment that gets UK hiring and investing again. 

####OECD employment rates, 2010-2019

!!1

In the long run, productivity gains are the fundamental source of improvements in prosperity. Productivity is closely linked to incomes and living standards and supports employment. Improvements in productivity free up money to invest in jobs and support our ability to spend on public services.

####UK productivity and total labour compensation, 2000-2019

!!2

Long-term investment in the country’s human and physical capital increases productivity. This means investing in people’s skills, the knowledge and capital within businesses, and the economic infrastructure that is the backbone of the economy, such as roads and broadband. We also need the right conditions to enable a dynamic economy that can encourage innovation and allow resources to be used most productively, which in practice means people being able to find high-quality work.

A long-term vision must be backed by action, and the plan for growth makes this possible by focusing on three pillars that are critical to supporting long-run growth and where UK action in the past has fallen short: infrastructure, skills and innovation.

The UK has had a long-standing productivity gap to other major economies - in 2019, the gap in output per hour worked between the UK and France was one percentage point different to the gap 40 years before[^9].

####GDP per hour worked in selected countries, 1979–2019

!!3

Much of the reason for this gap lies in historic low levels of investment in physical capital - from underinvestment in infrastructure and weaker business investment compared to our peers - and lower levels of basic and technical skills. In the last 20 years, average investment as a percentage of GDP has been the lowest in the G7[^10], and 18% of adults have a vocational qualification, compared to the OECD average of 27%[^11].

Within the UK itself, there are large disparities both across and within nations and regions, with only London and the South East with productivity above the UK average – this has implications for the relative prosperity of people living outside of those regions. 

Our commitment to Levelling Up means tackling these disparities, which are some of the widest of any advanced economy and have been getting wider over time. In 1998, London accounted for 20% of UK GVA, but by 2018 this had risen to 24%. Important explanations for these differences are the distribution of skills between regions, and cities outside London not fully capturing the benefits of their size.

####Relative contributions to national output of UK nations and regions

!!4

This is why a radical uplift in infrastructure investment and creating new skills training opportunities across the UK form two of the pillars of our plan for growth. Improving the UK’s performance in these areas would help to close the productivity gap between the UK and other countries, and close productivity gaps within the UK. 

Productivity growth has also slowed since the financial crisis. While the UK ranks highly as one of the most dynamic economies amongst its competitors, there are signs dynamism has been reducing and played a part in the slowdown. 

This is why the third pillar of the plan for growth is fostering the conditions to unleash innovation. This means maintaining a business environment where productive firms are able to scale up, barriers to business investment are removed and the constraints to new ideas and technology spreading through the economy are low.

###Building on the pillars of growth

The quality of our infrastructure is lower than many other countries. The UK Government has already announced a record amount of capital and infrastructure investment at the Spending Review 2020 and in the National Infrastructure Strategy[^12]. The plan for growth sets out how this investment will help the economy to recover, tackle our long-standing productivity gap, and lay the foundations for our long-term sustainable growth.

Skills play a crucial role in shaping people’s life chances. The UK has a strong foundation of advanced skills and a number of world-class universities, but lags behind international comparators on technical and basic adult skills. Improving skills opportunities will be important to productivity growth. The plan for growth sets out the range of interventions the UK Government is taking to drive improvements in skills. 

Innovation, including cutting-edge research and how businesses adapt their products and processes, will be an important part of improving UK productivity growth. The UK has a world-leading research base, and while we have some of the best companies in the world, we also have a lower proportion of innovative firms overall than other advanced economies[^13].  The plan for growth outlines measures that support and unlock innovative activity. These measures will support the high-growth businesses that make a significant contribution to employment and allow technology and best practice to diffuse through the economy to the benefit of all. 

Delivering improved infrastructure, skills and innovation will be a joint endeavour between local authorities, combined authorities, the devolved administrations and the UK Government. 

####Growth that delivers the people’s priorities

The plan for growth is targeted at building on the UK’s strengths and addressing weaknesses in order to boost economic growth and employment. However, we recognise that not all types of growth are equal. The focus of this government is to ensure the benefits of growth are spread to all corners of the UK, driving growth that delivers the people’s priorities:

- Levelling Up: the UK government’s most important mission is to unite and level up the country, improving everyday life for communities throughout the UK and ensuring everyone can succeed regardless of where they live. We will tackle geographic disparities in key services and outcomes, like health, education, and jobs; we will support struggling towns so they see social, economic and cultural regeneration; we want every region and nation of the UK to have at least one globally competitive city, acting as hotbeds of innovation and hubs of high value activity; and we will ensure that this plan builds on the strengths of the Union.

- Net Zero: the UK will continue to be at the forefront of tackling climate change and is already a world leader in clean growth. We will deliver the Ten Point Plan for a Green Industrial Revolution, leveraging significant private sector investment and supporting up to 250,000 highly-skilled jobs; we will take action to fulfil our commitment to be the first generation to leave the natural environment in a better condition than we found it; and we will ensure the finance sector can play its role to support the transition to net zero. 

- Global Britain: the UK’s prosperity is built on our integration into the global economic and financial system. Following our exit from the European Union, we can also take advantage of the opportunities that come with our new status as a fully sovereign trading nation. We will role model free and fair trade, ensure the UK remains a leading destination for global investment, and support opportunities for trade and collaboration.  


