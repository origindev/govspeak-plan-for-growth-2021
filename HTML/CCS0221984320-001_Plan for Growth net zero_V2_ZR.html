##Net zero

The UK has long been at the forefront of tackling climate change - the 2008 Climate Change Act was the first legislation in the world to provide a comprehensive framework to tackle global warming. In 2019 Parliament amended the act to commit the UK to net zero emissions by 2050. The UK is already a world leader in clean growth and has made considerable progress, reducing greenhouse gas emissions by over 40% between 1990 and 2019<a href="#endnote-001">1</a> while growing the economy by almost 80%. This has been achieved while minimising cost and causing a net reduction in average household energy bills.

####UK GDP vs greenhouse gas emissions

!!1

Reaching our upcoming carbon budgets and net zero target will require significant investment and is a major opportunity for economic growth and job creation across the country. The demand for low carbon goods and services will encourage new industries to emerge, with the potential to boost investment levels and productivity growth. Co-benefits from decarbonisation, such as improved air quality, can also be economically significant. 

Future policies will set a clear direction for millions of people and businesses, shifting incentives to favour low carbon technologies and tackling barriers to action. Moving decisively in areas of comparative advantage could generate new future-proofed jobs and export opportunities, and establish the UK as a global leader across the low-carbon economy. 

The Treasury’s Net Zero Review will further consider where the UK might have comparative advantages and how the UK can maximise the economic benefits from the transition. Policies like those in the Ten Point Plan for a Green Industrial Revolution will have an important role in ensuring the UK is able to make the most of those potential opportunities.

####Delivering our Ten Point Plan for a Green Industrial Revolution 

In 2020 we published the Ten Point Plan for a Green Industrial Revolution, setting out how the UK can make the most of the opportunities presented by the shift to net zero. This announced £12 billion of UK Government investment, and the ambition to leverage three times that amount of private investment by 2030 across key technologies such as hydrogen, offshore wind, nuclear, electric vehicles, heat and buildings. We will now focus on delivering the plan, which will support up to 250,000 highly-skilled green jobs, including through:

- producing enough offshore wind to generate more power than all our homes use today, quadrupling how much we produce to 40GW by 2030, supporting up to 60,000 jobs in 2030;

- working with industry, aiming to generate 5GW of low carbon hydrogen production capacity by 2030 for industry, transport, power and homes; and aiming to develop the first town heated entirely by hydrogen by the end of the decade;

- becoming a world-leader in technology to capture and store harmful emissions away from the atmosphere, with a target to remove 10Mt of carbon dioxide per year by 2030, equivalent to all emissions of the industrial Humber today, or taking around four million cars off the road. We will establish four CCUS (carbon capture, usage and storage) clusters, two by the mid-2020s and a further two by 2030;

- aiming for 600,000 heat pump installations per year by 2028, creating a market-led incentive framework to drive growth and innovation in low carbon heating, and bringing forward regulations to support this, especially in off-gas grid properties;

- ending the sale of new petrol and diesel cars and vans in 2030, backed by up to £2.8 billion to support the transition; investing £2 billion over this Parliament to make cycling and walking easier and safer; and committing £120 million in 21-22 for over 500 zero-emission buses

####Falling costs of renewable electricity since 2010

!!1

####Carbon Capture Usage and Storage and Low Carbon Hydrogen

- CCUS and hydrogen will play an important role in the UK achieving net zero emissions by 2050. In recognition of this, the Ten Point Plan announced an aim to capture 10Mt CO2/year using CCUS and, working with industry, generate 5GW of hydrogen, both by 2030.

- To help achieve these targets, at Spending Review 2020, we increased the size of the CCUS Infrastructure Fund to support the deployment of four CCUS clusters, two by the mid-2020s and a further two by 2030. This has the potential to support up to 50,000 jobs by 2030 in some of the country’s industrial heartlands, in areas such as the North East, the Humber, the North West, Scotland and Wales. The UK Government recently published further information on business models for CCUS<a href="#endnote-002">2</a>.

- The UK Government also announced £240 million for the low carbon hydrogen fund to help develop the UK’s hydrogen capacity, whilst supporting up to 8,000 jobs across the UK.We will bring forward a Hydrogen Strategy and hydrogen business models to drive private sector investment later in 2021.

####Ensuring the finance sector can play its role 

As the Chancellor set out in his vision for financial services, the UK Government’s ambition is for a greener industry, using innovation and finance to tackle climate change and protect our environment. The UK Government intends to fully implement a ‘Green Taxonomy’ to provide a common standard for measuring firms’ environmental impact and will require firms to disclose the climate risks they face in line with the recommendations of the Taskforce on Climate-Related Financial Disclosures (TCFD). We have joined the International Platform on Sustainable Finance to ensure standards can operate internationally. We are ensuring the remits of both the Monetary Policy Committee and the Financial Policy Committee reflect the UK Government’s economic strategy for delivering an environmentally sustainable net zero economy, and that market participants have the skills they need by launching the Green Finance Education Charter and its toolkit. We will issue the UK’s first sovereign green bond this summer, with a further issuance to follow later in 2021 as the UK looks to quickly build out a green yield curve in the coming years. Green gilt issuance in the financial year 2021-22 will be a minimum of £15 billion.

These green gilts will help fund critical projects to tackle climate change and other environmental challenges, finance important infrastructure investment, and create green jobs across the country. In addition, the UK will issue a green retail product linked to the green gilt programme, providing all UK savers with the opportunity to take part in the collective effort to tackle climate change.

####Prioritising our natural environment

We are taking action to fulfil our commitment to be the first generation to leave the natural environment in a better condition than we found it. The first round of the £80 million Green Recovery Challenge Fund is already supporting green jobs and nature recovery through 68 successful project applications. Meanwhile, the Nature for Climate Fund will scale-up tree planting and peatland restoration in England, harnessing the power of nature in support of net zero. We are taking advantage of our new freedoms to fundamentally reform our system of farm payments in England to prioritise climate and environmental benefits while boosting productivity and animal welfare. And we are progressing major waste reforms which will drive new jobs and investment in a more circular economy. 

####Creating deliverable plans to meet our targets

The Ten Point Plan has laid the foundation for a Green Industrial Revolution that will work with industry to help deliver our net zero target. Further progress on deliverable plans for sectors will follow in the coming year. The Energy White Paper built on the Ten Point Plan, providing further detail on how the energy sector can support the economic recovery while transitioning to net zero. Other sector strategies such as the Heat and Buildings Strategy, Transport Decarbonisation Plan and Industrial Decarbonisation Strategy will be published in due course, followed by the Net Zero Strategy later in the year. The Treasury’s Net Zero Review will consider further how the UK can maximise the economic benefits of the transition and ensure an equitable balance of contributions from households, businesses and taxpayers.

####Ensuring the route to net zero creates jobs

Regardless of the size or direction of the impact on the economy, the transition will lead to structural changes. Employment opportunities in green industries will emerge, while high carbon sectors will have to adapt. Some of these effects will be regionally concentrated. New green jobs are already appearing in sectors such as offshore wind, with growth and opportunities centred around regional clusters. Targeted investments in strategically important sectors will help enable a more sustainable long-term path, while protecting jobs and supporting levelling up across the UK.

<table class="financial-data source-tableeditor">
<thead>
<tr>
<th colspan="4">Case studies of sectors in transition to net zero</th>
</tr>
</thead>
<tbody>
<tr>
<td>Automotive - To support the UK&rsquo;s electric vehicle manufacturing industry, the UK Government has committed to spend nearly &pound;500 million in the next four years, as part of our commitment to provide up to &pound;1 billion for the development and mass-scale production of electric vehicle batteries and associated EV supply chain. This funding is available UK wide and will boost investment into the UK&rsquo;s strong manufacturing bases.</td>
<td>Aerospace - The Jet Zero Council is a partnership between industry and UK Government to drive the delivery of new technologies and innovative ways to cut aviation emissions. It launched in 2020, with the aim of delivering net zero aviation by 2050. Earlier this year UK Government and industry announced an &pound;84.6 million investment to develop zero-emissions flights, using alternative energy sources of hydrogen or electricity, which have the potential to unlock nearly 5,000 jobs across the UK.</td>
<td>Agriculture - The Agricultural Transition Plan published in November sets out our ambition for a renewed agricultural sector in which farming and the countryside make a significant contribution to our climate and environmental goals. To support this transition, we are maintaining the current annual budget to farmers throughout this Parliament while phasing in Environmental Land Management and other new schemes that will support increased agricultural sustainability and productivity.</td>
<td>Oil and Gas - The UK Government will provide &pound;27 million for the Aberdeen Energy Transition Zone, helping to support North East Scotland to play a leading role in meeting the UK&rsquo;s net zero ambitions. A further &pound;5 million for the Global Underwater Hub, on top of the &pound;1.3 million committed last year, and up to &pound;2 million to further develop industry proposals for the North Sea Transition Deal will support areas like Aberdeen transition to a low-carbon future.</td>
</tr>
</tbody>
</table>

The net impact of the transition on local labour markets will depend on the costs of decarbonising for individual firms and the flexibility of the labour market to match vacancies with the necessary skills. The investment and demand for low carbon goods and services will also require innovation and encourage new industries to emerge, creating jobs across the country. The Treasury’s Net Zero Review will assess how the UK can maximise economic growth opportunities from its transformation to a green economy, as well as mechanisms to create an equitable balance of contributions from households, businesses and taxpayers. The Green Jobs Taskforce forms part of the Ten Point Plan, and the government will work in partnership with businesses, skills providers and unions to develop plans as we transition to a high-skill, low carbon economy.

####Encouraging others to follow our lead

Tackling climate change and reversing biodiversity loss is a global challenge. As the recently published independent Dasgupta Review on the Economics of Biodiversity highlights, protecting and enhancing our natural assets, and the biodiversity that underpins them, will also be crucial to achieving a sustainable, resilient economies globally. The UK led the world in becoming the first major economy to legislate to eliminate our net contribution to climate change by 2050. It is critical that we work with our international partners to ensure the world follows our lead. Through our upcoming Presidency of COP26, and our Presidency of the G7, the UK Government is putting tackling climate change and environmental issues at the top of the international agenda. We have announced that we will end direct government support for the fossil fuel energy sector overseas, including trade promotion, export finance and aid funding, other than for a limited range of exceptions. To support our international partners to meet the global challenge, we have committed to providing at least £11.6 billion in International Climate Finance (ICF) support to developing countries between 2021 and 2025. As the world moves towards net zero, there will be opportunities for innovative UK industries to grow and export their expertise. 

####Our plan for net zero

<table class="financial-data source-tableeditor">
<thead>
<tr>
<th>Why net zero is important</th>
<th>What this means</th>
<th>What we&rsquo;re doing</th>
</tr>
</thead>
<tbody>
<tr>
<td>The right transition to net zero will present new opportunities for economic growth and job creation across the country and support our levelling up agenda.</td>
<td>A strong sustainable future economy.<br /><br />Up to 250,000 highly skilled green jobs in stable industries of the future.<br /><br />Net zero clusters in our industrial heartlands.</td>
<td>Aiming for 600,000 heat pump installations per year by 2028, creating jobs as we do it.<br /><br />Supporting up to 60,000 jobs in the offshore wind sector.<br /><br />Supporting up to 50,000 jobs in CCUS and up to 8,000 in hydrogen in our industrial clusters.<br /><br />Building net zero ready homes and expanding the 114,000 jobs already in the energy efficiency sector.</td>
</tr>
<tr>
<td>Building on our current net zero strengths will encourage new industries to emerge, growing demand for low carbon goods and services.</td>
<td>&pound;12 billion to support hydrogen, carbon capture and storage, offshore wind, nuclear, electric vehicles, heat and buildings.</td>
<td>Producing enough offshore wind to generate more power than all our homes use today, a target of 40GW offshore by 2030, including 1GW of advanced floating rigs.<br /><br />Working with industry to generate 5GW of low carbon hydrogen production capacity by 2030.<br /><br />Working with industry to capture 10Mt CO2 /year using CCUS by 2030, installing four CCUS clusters by 2030.<br /><br />Ending the sale of new petrol and diesel cars and vans in 2030, accelerating EV charging rollout and unlocking the potential of the growing UK charge point industry.</td>
</tr>
<tr>
<td>The private sector will lead the transition to net zero, within a policy environment designed to support investment and innovation.</td>
<td>The private sector will be responsible for most of the investment required to reach net zero.<br /><br />The Treasury&rsquo;s Net Zero Review is looking at how the UK Government can reduce policy uncertainty to encourage innovation, technological development and investment.</td>
<td>The new UK Emissions Trading Scheme creates a market incentive for the private sector to invest. We have committed to explore expanding the scheme to new sectors.<br /><br />Developing other levers such as regulation to &lsquo;crowd-in&rsquo; investment.<br /><br />Introducing the UK Infrastructure Bank to provide financing support and crowd in private capital to help develop and scale net zero infrastructure.<br /><br />Issuing a Green Gilt and building a green yield curve to build the Green Finance Market.<br /><br />Providing a common definition for green investment through a Green Taxonomy.<br /><br />Requiring firms to disclose the climate risks they face through TCFD.<br /><br />Ensuring financial regulators take climate change and the environment into consideration in exercising their statutory functions.</td>
</tr>
</tbody>
</table>


