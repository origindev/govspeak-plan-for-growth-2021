
##Skills

High quality education and skills training play a vital role in sustaining productivity growth and our international competitiveness: improvements in skills accounted for 20% of the UK’s productivity growth before the financial crisis.17

Creating opportunities to improve the skills of people in all regions is critical to the future success of the country and ensuring a strong recovery from the impacts of the COVID-19 pandemic, particularly for young people who have lost out on precious learning and employment opportunities. Improving our skills is also central to levelling up opportunity as differences in skill levels provide a key part of the explanation for differing output and wages across regions.

####The UK skills system has many strengths:

- Our university sector is world-leading: four of the world’s top twenty universities are in the UK<a href="#endnote-001">17</a>. This benefits our economy directly, with UK universities and their international students and visitors supporting over 940,000 jobs in 2014-15 alone<a href="#endnote-002">2</a>. 

- 52% of 25-34 year olds in the UK are educated to tertiary level, compared to an OECD average of 45%<a href="#endnote-003">3</a>.  

- At school level there have been substantial improvements: between 2005-2017 the proportion of the cohort achieving Level 2 qualifications with English and Maths by age 19 rose from 46% to 69% in England<a href="#endnote-004">4</a>.

####Relationship between skills and productivity, by region

!!1

The contribution of skills to productivity growth, however, can largely be attributed to higher-skilled cohorts<a href="#endnote-005">5</a>, and the UK’s skills system is less competitive internationally in areas such as technical skills and basic adult skills<a href="#endnote-006">6</a>. 

A particular challenge is the pipeline of technical skills: the UK has persistent technical skills shortages in key sectors such as construction and manufacturing. Only 4% of young people achieve a higher technical qualification by the age of 25, compared to 33% who get a degree or above<a href="#endnote-007">7</a>  Since the 2000s, higher technical education as a whole has fallen in absolute terms<a href="#endnote-008">8</a>.  

On basic skills, more than a quarter of the working-age population in England have low literacy or numeracy skills. The Industrial Strategy Council forecast that five million workers could become acutely under-skilled in basic digital skills by 2030<a href="#endnote-009">9</a>. This holds back those people from employment, limits their ability to progress, reduces economic growth, and makes the UK a less attractive place to invest. 

Combined, these factors mean that there are significant levels of mismatch between what the skills system provides and what employers need: the OECD found that the UK could improve its productivity by 5% or more if it reduced the level of skills mismatch to that of high performing international comparators<a href="#endnote-010">10</a>.  Reforms to the technical skills system to support providers to respond to employer needs will play an important role in addressing these mismatches and driving growth at a national and regional level. 

Overall, while there are notable strengths in the UK skills system, such as universities and apprenticeships, further reform is needed to address these challenges. The UK Government has put in place a package of immediate employment and skills support delivered through the Plan for Jobs, and is taking broader action to ensure people have the support they need to improve their skills over the longer term, as a key enabler of productivity growth. 

###Transforming Further Education

Recognising the importance of technical education, the UK Government has invested significant additional funding in Further Education (FE) in England, with an additional £691 million for core 16-19 education at the last two Spending Reviews, and a further £375 million to fund technical education for adults as the first step in a £2.5 billion investment in adult skills over the course of this Parliament through the National Skills Fund. The UK Government has also made significant capital commitments in post-16 education, with £1.5 billion over six years to raise the condition of the FE college estate, £270 million to establish 20 Institutes of Technology, £268 million across three waves to support the roll-out of T Levels with high quality equipment and facilities, and £83 million in 21/22 to ensure post-16 providers have sufficient places.  

The UK Government is reforming technical education, making it a true alternative to a degree by delivering the training and education that employers want. 

This includes aligning the substantial majority of post-16 technical education and training to employer-led standards, introducing new approved Higher Technical Qualifications as a high quality technical progression option, rolling out T Levels (see box below) and expanding the flagship Institutes of Technology programme to every part of the country by the end of this Parliament – to spearhead the increase in higher-level technical skills in Science, Technology, Engineering and Maths. 

####T Level rollout: 

T Levels will be the option of choice in England for the majority of 16 to 19 year-olds who want to progress into high-skilled employment or onto higher levels of technical education at college or university. Students spend most of the course in classrooms and specialist training facilities, developing the knowledge, skills and behaviours that employers need for their chosen occupation. They also spend at least 45 days on a meaningful industry placement, putting the skills they have developed into practice and gaining first-hand experience of industry. Around 300 employers have been involved in designing content, and thousands more will be offering industry placements. Students across England started on the first ever T Levels in September 2020; from 2023, we expect that 24 T Levels covering 11 technical education routes will be available.

To ensure that skills provision is aligned to employer needs, the UK Government has published Skills for Jobs: Lifelong Learning for Opportunity and Growth which sets out how we will reform further education in England so it supports people to develop the skills throughout their lives, wherever they live in the country<a href="#endnote-011">11</a>. This reform will be delivered by putting employers at the heart of our skills system, supporting excellent teaching in further education, and reforming funding and accountability to ensure a focus on the needs of local labour markets, enabling flexible employer-led provision, and reducing unnecessary bureaucracy. 

####Encouraging lifelong learning 

Recognising the vital role of training and retraining over the course of an adult’s lifetime, and as the Prime Minister set out in his speech in September 2020, the UK Government will help people get the skills they need at every stage in their lives through the Lifetime Skills Guarantee. 

These reforms are backed by significant investment in adult skills. The UK Government has committed to a new £2.5 billion National Skills Fund over the course of this Parliament to improve the technical skills of adults in England. The UK Government will also introduce the UK Shared Prosperity Fund (UKSPF) to help to level up and create opportunity across the UK for people and places. A portion of the UKSPF will be targeted at bespoke employment and skills support tailored to local need.

####Lifetime Skills Guarantee

The Prime Minister has introduced a Lifetime Skills Guarantee to give people access to the education and training they need throughout their lives. 

Starting from April 2021, adults looking to achieve their first full advanced level (level 3) qualification, which is equivalent to an advanced technical certificate or diploma, or two full A levels, will be able to access a free, fully funded course as part of the Lifetime Skills Guarantee. These courses, in areas including business, engineering, health and social care, and digital, will improve people’s employment prospects and open up new opportunities. 

Our employer-led skills bootcamps will also be rolled out across England from April as part of the Lifetime Skills Guarantee building on the successful digital bootcamps pilots. Skills bootcamps are 12 to 16-week training courses linked to guaranteed job interviews that support adults to retrain, top up skills, or gain new specialist skills in areas that are in-demand by employers in a bitesize and flexible way.

We will reform student finance to provide individuals with a flexible Lifelong Loan Entitlement from 2025, the equivalent of four years of post-18 education. The loan entitlement will be useable for both shorter modules and full years of study at higher technical and degree levels (levels 4-6), regardless of whether they are provided in colleges or universities, making it easier for people to study more flexibly throughout their lifetime.

####Building on the apprenticeships revolution 

The UK Government has transformed apprenticeships in England to ensure that they better meet the skills needs of employers across the country and provide people of all ages and backgrounds with the opportunity to benefit from a high-quality apprenticeship.  

The Apprenticeship Levy is an important part of these changes as it encourages employers to make a long-term, sustainable and high-quality investment in apprenticeship training. As part of these changes, standards have helped make apprenticeships employer-led, and we have seen over 440,000 apprentices start on the new standards between the 2014/15 and 2018/19 academic years<a href="#endnote-012">12</a>. 

Building on these reforms, we will continue to focus on the quality of apprenticeships and take steps to improve the English apprenticeship system for employers. Spending Review 2020 announced the following: 

- From August 2021, employers who pay the Apprenticeships Levy will be able to transfer unspent levy funds in bulk to small and medium-sized enterprises (SMEs) with a new pledge function. We will build on good examples of this activity already in existence – for instance the West Midlands Levy Transfer Fund – by also introducing, from August 2021, a new online service to match levy payers with SMEs that share their business priorities. 

- From April 2021, the UK Government will allow English employers in construction, health and social care to front-load apprenticeship training, and will explore whether this offer can also be made available in other sectors.

Building on the measures the Government announced at Spending Review 2020 to improve the apprenticeship system for employers, the Budget outlines a £7 million fund from July 2021 to help employers in England set up and expand portable apprenticeships. This will enable people who need to work across multiple projects with different employers to benefit from the high-quality long-term training that doing an apprenticeship provides. Employers themselves will also benefit from access to a diverse apprenticeship talent pipeline. 

The UK Government will also extend and increase the payments made to employers who hire new apprentices. Employers in England who hire a new apprentice between 1 April 2021 and 30 September 2021 will receive £3,000 per new hire, compared with £1,500 per new apprentice hire (or £2,000 for those aged 24 and under) under the previous scheme. This is in addition to the existing £1,000 payment the government provides for all new 16 to 18 year-old apprentices and those aged under 25 with an Education, Health and Care Plan, where that applies. 

####West Midlands Levy Transfer Fund: 

Employers who pay the Apprenticeship Levy can transfer up to 25% of their unspent levy funds to small to medium-sized businesses who share their business priorities. The West Midlands Combined Authority (WMCA) set up a scheme in 2018 to facilitate these transfers and supercharge apprenticeships in the West Midlands. Employers like Lloyds Banking Group, BBC, National Express and the University of Birmingham have partnered with the WMCA to transfer their unspent levy to smaller employers who want to offer apprenticeships in key skills areas. The fund recently hit a £5 million milestone and has supported over 1,000 new apprenticeships at small to medium-sized businesses across the region. 

####Our plan for skills

<table class="financial-data source-tableeditor">
<thead>
<tr>
<th>Why skills are important</th>
<th>What this means</th>
<th>What we&rsquo;re doing</th>
</tr>
</thead>
<tbody>
<tr>
<td>Skills and training is central to recovery from the COVID-19 pandemic by supporting people into work.<br /><br />Access to high quality training is vital to levelling up.</td>
<td>Providing individuals affected by the crisis with the opportunity to build the skills they need to boost their job prospects.</td>
<td>Expanding traineeships and improving their quality and progression to apprenticeships; expanding sector-based work academies; incentivising new apprenticeship hires; and boosting the National Careers Service&rsquo;s capacity.</td>
</tr>
<tr>
<td>High quality education and skills training play a vital role in sustaining productivity growth.</td>
<td>Boosting investment in technical education and adult skills, and reforms to better align the skills system with employer demand will support productivity growth.</td>
<td>Investing additional resource and capital funding in Further Education in England.<br /><br />Reforming technical education, including aligning the post-16 technical education system more closely with employer demand.<br /><br />Continuing to focus on the quality of apprenticeships and improving the apprenticeship system for employers.</td>
</tr>
<tr>
<td>80% of the workforce of 2030 are already in work today, and we need to offer them the opportunity to upskill and reskill over their careers to progress and adapt to changes such as automation.</td>
<td>Providing adults with opportunities to upskill and reskill throughout their lifetime in a way that meets their needs (i.e. is flexible and provides line of sight to a job or career progression).</td>
<td>Improving the apprenticeship system for employers building on the all ages, all levels approach.<br /><br />Funding for adults without a Level 3 qualification (A levels or equivalent) to take courses in areas that will help boost their job prospects.<br /><br />Rolling out employer-led skills bootcamps across England for adults to upskill and reskill in a flexible, bitesize way.<br /><br />From 2025, delivering the Lifelong Loan Entitlement to make it easier for adults and young people to study more flexibly throughout their lifetime.<br /><br />In the interim, consulting on the scope and detail of the entitlement - and taking action to stimulate higher technical and modular provision and encourage more frequent credit transfer.<br /><br />Skills and Employment funding in the new UKSPF programme.</td>
</tr>
</tbody>
</table>


