
##Infrastructure 

High quality infrastructure is crucial for economic growth, boosting productivity and competitiveness. More than this, it is at the centre of our communities. Infrastructure helps connect people to each other, people to businesses, and businesses to markets, forming a foundation for economic activity and community prosperity. Well-developed transport networks allow businesses to grow and expand, enabling them to extend supply chains, deepen labour and product markets, collaborate, innovate and attract inward investment. Digital connectivity is unlocking new and previously unimaginable ways of working, and is now essential to facilitate public services, including healthcare and education.

The government is committed to transforming the UK’s infrastructure and increased investment is also a central part of economic recovery. The COVID-19 pandemic has introduced enormous short-term disruption and may have long-term effects on the way people live, for instance with less daily commuting. However, this does not undermine the long-term arguments for infrastructure. Instead, it requires the government to be flexible and adapt to the country’s changing needs. Investment in digital, transport and utilities networks is still required to underpin economic recovery and growth. 

A 10% increase in the public capital stock – a measurement of the value of the UK’s current infrastructure networks – has been linked to a 1-2% increase in GDP, partly through productivity improvements[^14]. 

Infrastructure can also support other UK Government policy objectives. For instance, it can improve skills and education through investment in digital technology and buildings, and health outcomes through investment in our health infrastructure. It is a key factor in determining where firms choose to locate and grow, and people’s ability to access resources. It unlocks development of housing. It can also support and enable our businesses to integrate into the international economy and trade goods and services across the world.

Last year, the UK Government published the National Infrastructure Strategy (NIS)[^15], which brought together the government’s long-term infrastructure priorities with the short-term imperative to build back fairer, faster and greener following the COVID-19 pandemic. The NIS committed to: 

- Boosting growth and productivity across the whole of the UK, levelling up and strengthening the Union through investment in rural areas, towns and cities, from major national projects to local priorities. 

- Putting the UK on the path to meeting its net zero emissions target by 2050 by taking steps to decarbonise the UK’s power, heat and transport networks – which together account for over two-thirds of UK emissions – and take steps to adapt to the risks posed by climate change. 

- Supporting private investment by providing investors with clarity over the UK Government’s plans, so they can look to the UK with confidence and help deliver the upgrades and projects needed across the country.

- Accelerating and improving delivery through wide-ranging Project Speed reforms including streamlining the planning system; improving the way projects are procured and delivered; and greater use of cutting-edge construction technology.

The National Infrastructure Strategy was just the first step of a multi-year process to transform the UK’s infrastructure networks and the UK Government is now focused on implementing this vision. 

###Delivering historic levels of infrastructure investment

The UK has historically underinvested in infrastructure, with a smaller capital stock than comparable countries and ranking 11th globally for infrastructure quality, behind both France and Germany[^16]. We are fixing that – Spending Review 2020 committed £100 billion of capital investment in 2021-22, a £30 billion cash increase compared to 2019-20. These plans make progress on delivering the UK Government’s objective of over £600 billion of gross public sector investment over the next five years.  

The private sector also plays a vital role in achieving the UK’s infrastructure ambitions. Much of UK’s economic infrastructure is privately owned, with almost half of the UK’s future infrastructure pipeline forecast to be privately financed. Over the past decade alone, over £200 billion has been invested in the water and energy sectors. However, historic levels of investment will be required in UK infrastructure in the coming years, to maintain and upgrade networks to meet the UK Government’s objectives for economic growth and decarbonisation. 

####UK Government capital departmental expenditure limits (CDEL) outturn and plans, 2015-16 - 2025-26

!!1

###Building back better, greener and faster

The UK Government needs to deliver infrastructure projects better, greener and faster. That means addressing longstanding challenges such as complex planning processes, slow decision-making, government capability and low productivity in the construction sector.  

As set out in the Prime Minister’s Ten Point Plan for a Green Industrial Revolution, infrastructure investment is fundamental to delivering net zero emissions by 2050. The UK Government will unlock private sector investment to accelerate the deployment of existing technology, such as retrofitting the UK’s building stock and electrification of vehicles, while advancing newer technologies such as carbon capture and low carbon hydrogen. The UK Government’s approach will create jobs to support the recovery from COVID-19 and support the UK Government’s levelling-up agenda by ensuring key industrial areas are at the heart of the transition to net zero. As Glasgow hosts the UN Climate Change Conference COP26 next year, the UK will go even further to promote the importance of low carbon infrastructure and support its commitment to the Paris Agreement.

Our exit from the EU also provides a transformational opportunity to change how this government delivers infrastructure projects, using the flexibility the UK has as a sovereign country to do things differently. The government wants the UK to have the most efficient, technologically advanced and sustainable construction sector in the world. In June last year, the government set up Project Speed to review every part of the infrastructure project lifecycle and identify where improvements could be made, such as on procurement.  

As a part of Project Speed, we will rigorously review the cost and delivery times of infrastructure projects. We will transform the way infrastructure is done in this country: it will be more efficiently delivered, driven by new technologies and less bureaucracy. It will be built better, with the construction of world-class schools and hospitals. Infrastructure will also be greener to align with our net zero ambitions.  

These reforms will have a material impact on project timelines, bringing forward real benefits to local communities, business and the wider economy across the UK. The A66 Northern Trans-Pennine upgrade connecting the North East and North West of England will be accelerated through Project Speed and is expected to be delivered five years sooner through radical acceleration of the construction schedule. The Budget confirms £135 million development phase funding to get spades in the ground by 2024. 

Similar rigorous Project Speed approaches and mindsets will be applied to other critical regional and national projects over this Parliament.

####Project Speed Pathfinders

The Project Speed taskforce has focused on a number of high-profile “pathfinder projects” to identify reforms which could accelerate and improve delivery across the UK Government’s infrastructure portfolio. This is an evolving subset of projects, which is kept under review, but examples of current pathfinder projects include:

- The Oxford-Cambridge Arc, led by the Ministry of Housing, Communities & Local Government (MHCLG) 

- The New Hospital Programme within the Health Infrastructure Plan, led by the Department of Health & Social Care (DHSC)

- The A66 Northern Trans-Pennine upgrade, led by the Department for Transport (DfT)

- The Northumberland Line, led by Northumberland County Council in partnership with DfT

###Transforming journeys, communities and the environment

The UK Government is implementing the National Infrastructure Strategy and investing in infrastructure to transform delivery and support private investment. This includes:

- over £22 billion for HS2, which will form the spine of the UK’s transport network by delivering essential North-South connectivity between some of the UK’s biggest and most productive cities; 

- £1.3 billion to accelerate the rollout of electric vehicle charging infrastructure, £5 billion for buses and cycling and our forthcoming National Bus Strategy.

- £1 billion of UK-wide funding to support the establishment of carbon capture and storage in four industrial clusters;

- £4.2 billion for intra-city transport settlements to support our largest city regions;

- £5 billion to accelerate UK-wide gigabit broadband roll-out, a Shared Rural Network extending 4G mobile coverage to 95% of the UK and £250 million to ensure resilient and secure 5G networks and £50 million for the continuation of the 5G Testbeds and Trials Programme in 2021-22;

- new UK-wide funds: the £4.8 billion Levelling Up Fund, and the £150 million Community Ownership Fund (over four years from 2020-21) that will invest in local infrastructure;

- £5.2 billion by 2027 to better protect communities from flooding and coastal erosion; 

- record amounts of funding provided for national road and rail, including the Lower Thames Crossing – boosting capacity east of London by 90%

The Union Connectivity Review will assess how the quality and availability of transport infrastructure across the UK can support economic growth and quality of life across the whole of the UK. The review will deliver recommendations in Summer 2021 that set out how best to improve transport connectivity across the UK in the long-term, including bolstering existing connections. These recommendations will be consistent with the UK’s wider fiscal strategy. This will help us to consider future strategic investment to better connect our Union.

The UK Government is soon to publish a White Paper setting out the future of the railways. This will respond to the recommendations of The Williams Rail Review into rail industry reform.

While the UK Government will be investing record amounts in infrastructure, the private sector will continue to play a leading role. To facilitate this, the government is establishing a new UK Infrastructure Bank in the North of England to bring together public and private support for new infrastructure. The new UK Infrastructure Bank will provide financing and advisory support to local authority and private sector infrastructure projects across the United Kingdom to help meet government objectives on climate change and regional economic growth. It will be able to deploy £12 billion of equity and debt capital and be able to issue up to £10 billion of guarantees. Further details on the mandate and scope for the bank are set out in the UK Infrastructure Bank Policy Design document, published alongside Budget 2021.  

Maintaining and building on the UK’s high-quality system of economic regulation will be crucial to attracting private investment. In the National Infrastructure Strategy, the government reaffirmed its belief in themodel of independent economic regulation and committed to refining it, to ensure that it provides a clear and enduring framework for investors and businesses and delivers the major investment needed in decades to come, whilst continuing to deliver fair outcomes for consumers. The UK Government will publish a policy paper on the system of economic regulation, focused on providing a clear and predictable framework which can rise to the challenges and opportunities of the 21st century. As part of this, the UK Government will consider regulator duties to ensure they are coherent, relevant and effective, as well as exploring the benefits of a cross-sector strategic policy statement. 

###Investing in places

The UK Government’s commitment to ensuring infrastructure investment delivers regional economic growth means we will be investing in: 

- City and Growth Deals for Scotland, Wales and Northern Ireland. To accelerate local economic priorities, £25.8 million will be brought forward over the next five years for three City and Growth Deals in Scotland (Ayrshire, Argyll & Bute, and Falkirk) and £58.7 million will be brought forward over the next five years for three City and Growth Deals in Wales (Swansea, north Wales and mid-Wales). The UK Government is also investing £617 million in four City and Growth Deals in Northern Ireland. 

- Intra-city transport settlements. Budget 2020 committed to invest £4.2 billion in these from 2022-23, through five-year consolidated funding settlements for eight city regions, including Greater Manchester, Liverpool City Region, West Midlands, West Yorkshire, Sheffield City Region, West of England and Tees Valley, subject to the creation of appropriate governance arrangements to agree and deliver funding. This Budget takes the first step toward delivering the government’s commitment - confirming capacity funding in 2021-22 to support those city regions with appropriate governance arrangements already in place to begin preparations for settlements, enabling them to develop integrated investment-ready transport plans that will deliver on local priorities such as tackling congestion and driving productivity.

- The Oxford-Cambridge Arc, where a Spatial Framework, developed with community engagement at its core, will set the long-term, holistic strategy for infrastructure investment to support jobs, unlock clean growth, and achieve net zero alongside environmental sustainability; cultivating the Arc’s potential to become a global innovation powerhouse. Earlier this year the government confirmed funding for the next stage of East West Rail, which will connect communities and create jobs. We are also exploring up to four development corporations along its route which can help deliver sustainable, beautiful places to live and work for existing and future communities.

- The Integrated Rail Plan for the Midlands and the North will ensure that Phase 2b of HS2, Northern Powerhouse Rail and other planned rail investments in the North and Midlands are scoped and delivered in an integrated way, bringing transformational rail improvements more quickly and to more places.

- Freeports, where the UK Government is announcing that eight locations have been successful in the Freeports bidding process for England. Subject to the successful completion of their business case assessments, these Freeports will begin operations from later in 2021. Freeports will benefit the whole of the UK, not just England, and we are determined to deliver this as soon as possible. That is why we are continuing to work constructively with the devolved administrations in Scotland, Wales and Northern Ireland to establish at least one Freeport in each nation as soon as possible.    


####Local infrastructure schemes

City and Growth Deal (UK wide)

Flood Defences (England only)

Housing Infrastructure Fund - Forward funding (England only)

Mayoral Gainshare (England only)

Regeneration Project (England only)

Towns Fund (England only)

!!1

Note: Where policy is reserved for the UK government – for example digital infrastructure – it is taking action to improve infrastructure across the whole of the UK. Where policy is devolved – for example substantial areas of transport – the UK government allocates funding to the devolved administrations through the Barnett formula. This map shows how investment by the UK government in a number of local infrastructure programmes will benefit different regions.

####Our plan for infrastructure

<table class="financial-data source-tableeditor">
<thead>
<tr>
<th>Why infrastructure is important</th>
<th>What this means</th>
<th>What we&rsquo;re doing</th>
</tr>
</thead>
<tbody>
<tr>
<td>Investing in infrastructure drives long term productivity improvements, and in the short term stimulates economic activity.</td>
<td>A 10% increase in the public capital stock has been linked to a 1-2% increase in GDP.</td>
<td>Record investment in broadband, flood defences, roads, rail and cities &ndash; as part of our capital plans worth &pound;100 billion next year.<br /><br />Project Speed established to accelerate and improve delivery, delivering impacts by the end of the Parliament.<br /><br />&pound;4.8 billion invested at Spending Review 2020 for infrastructure, land remediation and land assembly to unlock housing.</td>
</tr>
<tr>
<td>The private sector has a big role alongside the government in increasing investment and improving economic outcomes</td>
<td>Almost half of the UK&rsquo;s future infrastructure pipeline is forecast to be privately financed.</td>
<td>A new UK Infrastructure Bank to &lsquo;crowd-in&rsquo; investment and boost the pipeline of projects.</td>
</tr>
<tr>
<td>Boosting infrastructure investment in all parts of the country will help connect people to opportunity across the UK and help areas to level up.</td>
<td>People see tangible improvements in their local area, feel more pride in their communities and believe they can succeed wherever they live.</td>
<td>A new Levelling Up Fund, Shared Prosperity Fund, Towns Fund and High Street Fund to invest in local areas.</td>
</tr>
<tr>
<td>Infrastructure investment will be central to meeting our net zero objectives.</td>
<td>Over 80% of UK emissions come from infrastructure sectors: power, heat, heavy industry and transport networks.</td>
<td>&pound;12 billion of funding for projects through the Prime Minister&rsquo;s Ten Point Plan for a Green Industrial Revolution.</td>
</tr>
</tbody>
</table>



