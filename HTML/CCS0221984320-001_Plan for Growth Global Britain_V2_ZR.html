##Global Britain

Following our exit from the European Union, an independent Global Britain can take advantage of the opportunities that come with our new status as a fully sovereign trading nation. We have the opportunity to reinvigorate international cooperation and institutions, working with others to tackle global challenges head on.

!!1

The UK’s prosperity is built on our integration into the global economic and financial system. An open economy, which permits the free flow of ideas, goods, services and data based on adherence to a mutually agreed set of rules and principles, will drive long-term prosperity and innovation. It provides UK consumers, businesses, producers, workers and investors with access to cheaper, better quality goods and services, offering greater choice, creating jobs and freeing up resources for innovation and investment at home. In 2019-20, new inward investment projects supported over 56,000 jobs across the UK<a class="_idEndnoteLink _idGenColorInherit" href="#endnote-001">1</a>.

The UK’s success as a trading nation will depend on its ability to use its comparative strengths to anticipate evolving demand at both a country and sector level. Trends such as continued rapid growth in emerging economies, the expansion of the global middle class, as well as the growing demand and increasing tradability of more sophisticated sectors of the global economy all provide potential opportunities for UK businesses. Openness to international markets ensures UK access to multiple diverse sources of supply for the goods and services we need, improving the resilience of our supply chains and benefitting prosperity. According to the OECD, a 10 percentage point increase in trade exposure is associated with a 4% increase in income per head.

However, the open global economy is facing significant challenges. Global economic growth has been slower since the 2008-09 financial crisis and the pace of globalisation – openness to trade and investment – has stalled. Momentum for trade liberalisation is slowing and cases of protectionism increasing, in part due to states’ more aggressive use of trade policy as a lever in international competition. COVID-19 may accelerate the trend towards more regional and national approaches, although trade flows recovered relatively quickly following the initial shock. 

We will use our global influence and all the policy levers available to us to demonstrate the benefits of openness, propose and develop solutions to challenges facing the international order. We will build agile alliances and coalitions of likeminded partners to create global action that will deliver on our domestic priorities.

As the upcoming Integrated Review of Security, Defence, Development and Foreign Policy (Integrated Review) will set out, our international actions support and underpin our domestic goals. Our infrastructure, skills, and innovation will benefit from an open and resilient international system and our integration within it. At the same time, our global leadership derives strength from our ambitious positions on the issues of the day, including important priorities such as climate, digital, or services. 

###Demonstrating international leadership on our domestic priorities

The Integrated Review will set the vision for Global Britain to 2030. In the context of our plan for growth this means working with international partners and cooperating on global issues in the interest of our domestic priorities for growth. We will take leading positions and encourage global action on the most important issues in the international environment, and advance UK priorities to build back better including: 

- Economic recovery and future prosperity: we will work with partners, including through our G7 Presidency and our trade programme to develop a shared agenda for international and national economic recoveries that deliver jobs, prosperity and wellbeing for all our citizens in a way that previous recoveries from international crises have not. 

- Health: we will provide continued leadership in the global response to COVID-19, charting the way out for our citizens and the planet, whilst seizing the opportunity to better prepare for any future pandemics. We will show continued leadership in closing the COVAX funding gap, as well as ensuring secure supply of medicines and critical medical products, building on our recent suspension of tariffs for COVID-19 critical products. The UK has committed to share the majority of any future surplus COVID-19 vaccines from our supply with the COVAX procurement pool to support developing countries, in addition to the UK’s £548 million funding for the scheme. 

- Climate change and biodiversity: We are committed to placing climate and nature at the heart of the multilateral agenda. We will:
  - Demonstrate global leadership as we convene COP26 discussions in Glasgow, including by taking ambitious action to meet our Paris Agreement commitments and reverse global biodiversity loss. 
  - Encourage partners to follow our lead with ambitious commitments to net zero and significantly increase their climate finance commitments. 
  - Examine and respond to the findings of the Dasgupta Review on the Economics of Biodiversity, and encourage international partners to do the same.

###Role modelling free and fair trade

We can encourage and inspire other countries to act through our domestic actions. By demonstrating the prosperity and innovation benefits to those that would follow our lead, we lend credibility to our work to uphold and improve the international trading system.

By taking a twin track approach we will position the UK as a global service, digital and data hub to seize international opportunities to drive domestic growth:

- We will work to strengthen the international trading system and reform the WTO to update the global trading rules in support of free and fair trade and progressing further liberalisation, promoting openness and cooperation in the face of collective challenges, including through our G7 Presidency. 

  - We will work with partners to break the current impasse at the WTO, developing a shared approach on Dispute Settlement Mechanism reform, WTO rulemaking and tackling unfair practices.

  - We will support the development of global principles, norms and standards on those emerging areas that are not fully part of the existing international rulebook, at the frontier of the future global economy in areas such as services, digital and data.

  - We will leverage our reputation as a trusted regulatory leader and innovator to influence global rules, norms, and standards to improve regulatory outcomes and market access for our firms globally.

- Using preferential agreements and bilateral trade relationships to promote an open international order, opening markets and driving domestic prosperity through new opportunities for UK businesses.

  - We have agreed the Trade and Cooperation Agreement (TCA) with the EU, the first free trade agreement the EU has ever reached based on zero tariffs and zero quotas – a deal that will help unlock investment and protect high value jobs right across the UK. We are continuing to support businesses to make best use of these new arrangements.

  - We have transitioned trade agreements with 64 countries, securing existing relationships preferences and supply chains. This includes agreements with Japan, Turkey, Vietnam and Singapore, which together accounted for £71 billion of UK trade in 2019. We will put the UK at the centre of a network of modern deals spanning the Americas and Pacific. Negotiations are currently ongoing with Australia, New Zealand, and the US on deals that will have significant opportunities for the whole of the United Kingdom. We are seeking to join the Comprehensive and Progressive Agreement for Trans-Pacific Partnership (CPTPP), recognising the growing importance of the Indo-Pacific to the UK economy, and supporting our aim to agree trade agreements covering 80% of UK trade by 2022. We have also launched an Enhanced Trade Partnership with India. 

  - We will deepen our bilateral relationships and remove market access barriers outside of trade agreements, for example through our successful engagement with the US to lift the historic ban on UK beef imports, and through seeking to negotiate agreements such as the UK-Switzerland Mutual Recognition Agreement on financial services and the UK-Singapore Digital Economy Agreement.

  - Official Development Assistance will help build trading and investment partners of the future. We will focus on those partners where UK development, security, and economic interests align, helping countries to trade and creating better investment environments, infrastructure and access to finance.

  - We will continue Economic and Financial Dialogues with China, India and Brazil to discuss bilateral and multilateral economic issues, and to support increased financial services cooperation and trade and investment with these major emerging markets.

####Case study: UK Global Tariff

On 31 December 2020 the UK Government implemented the UK Global Tariff (UKGT). Replacing the EU’s Common External Tariff (CET), it is the UK’s first independent tariff schedule in almost 50 years and applies to all goods imported from trading partners that we do not have a preferential trading agreement arrangement with (such as a Free Trade Agreement). 

The UKGT was designed to support growth:  it is generally simpler, easier to use and lower than the EU’s CET, and it is in pounds not euros. It has been tailored to the needs of the UK economy, supporting businesses and their supply chains by making it easier and cheaper to import goods from overseas. 

The UKGT almost doubles the number of tariff lines that have zero import tariffs relative to the CET – with just under 50% of products lines now zero-rated compared to 27% in the EU’s CET. Specifically, the UKGT: 

- removes tariffs on around £30 billion worth of imports of inputs to production, supporting supply chains for UK businesses;

- removes tariffs on £10 billion worth of imports that have little to no UK production, lowering costs for UK consumers<a class="_idEndnoteLink _idGenColorInherit" href="#endnote-002">2</a>;

- removes tariffs on 100 ‘green goods’ to promote a sustainable economy, e.g. removing tariffs on thermostats, vacuum flasks and LED lamps;

- removes all ‘nuisance tariffs’ below 2%, e.g. on fire extinguishers and school pencils, cutting down on red tape;

- simplifies by rounding tariffs down into set bands (e.g. reading glasses from 2.9% to 2% and alarm clocks from 4.7% to 4%) and removing the complex tariff calculations

The UK Government has sought to balance objectives by maintaining tariffs on certain sensitive products, to underpin our free trade agreement ambitions and to protect preferential access for developing nations. This includes certain agricultural products, road vehicles and ceramics.

###Ensuring the UK remains a leading destination for global investment

Inward investment brings economic benefits to the UK – in 2019-20 inward investment created over 56,000 jobs across the UK, of which over three quarters were outside London<a class="_idEndnoteLink _idGenColorInherit" href="#endnote-003">3</a>– and it can also play a role in supporting the UK Government’s key priorities, such as levelling up and net zero. The UK is open for investment and continues to encourage and support investors from around the world. The Government is committed to ensuring that the UK continues to be one of the top destinations in the world for investment. 

To achieve this, we are reaffirming the UK Government’s commitment to maintaining the strengths that make the UK an attractive location for investment. The UK is consistently ranked in the top 10 of globally respected investment indices, including the World Economic Forum’s Global Competitiveness Index (GCI). This reflects the UK’s competitive environment, including:

- a high skilled workforce with the UK ranked 5th globally for our ability to grow talent<a class="_idEndnoteLink _idGenColorInherit" href="#endnote-004">4</a>; 

- world-leading research institutions with four of the world’s top 20 universities based in the UK<a class="_idEndnoteLink _idGenColorInherit" href="#endnote-005">5</a>;

- our reputation for innovation with the UK ranked fourth globally in 2020<a class="_idEndnoteLink _idGenColorInherit" href="#endnote-006">6</a>;

- a highly regarded legal system and reputation for upholding the coming years rule of law<a class="_idEndnoteLink _idGenColorInherit" href="#endnote-007">7</a>;

- the UK’s position as a global financial centre

The UK Government has established a new Office for Investment which is now operational. It will support high value investment opportunities into the UK which align with key government priorities, including our focus on key sectors, reaching net zero, investment in infrastructure and advancing research and development.

The GREAT Britain and Northern Ireland campaign showcases all four nations to encourage people to visit, do business, invest and study in the UK.

####Supporting UK businesses to participate in trade

We will make use of the full range of policy tools available to us as an independent trading nation to improve our openness to trade and investment, supporting our key growth sectors, on the basis of free and fair competition. Exporting will play a key role in developing our pioneering sectors and technologies to become world-leading, driving growth and creating jobs. As part of this, we will support UK business to make the most of the opportunities that come with exporting and participating in an integrated global trade and investment system through:

- A UK tariff policy tailored to the needs of UK businesses and consumers, almost doubling the amount of tariff lines that are zero-rated relative to the EU Common External Tariff, upholding our principles and supporting businesses by eliminating nuisance tariffs, removing tariffs on inputs to production, and simplifying the schedule. 

- A refreshed Export Strategy to align our support for exporters with our plan for growth and sectoral priorities.

- New Trade and Investment Hubs in Scotland, Wales and Northern Ireland to help businesses across all parts of the UK to grow and thrive internationally.

- A £38 million Internationalisation Fund to support up to 7,600 SMEs to grow their overseas trading and strengthen their business.

- Our HM Trade Commissioners, who will continue to champion our trade around the world, supporting businesses to trade internationally and promoting the UK as a place to invest.

- The Export Academy, which will provide a series of activities to build the capabilities of smaller businesses to trade internationally.

- Expanded UK Export Finance support, comprising:

- Increased lending with £8 billion overall capacity, including £2 billion dedicated to clean growth exports.

- New Export Development Guarantee and General Export Facility products launched providing working capital support for large and small exporters.

- Freeports, comprising tax incentives to boost investment, easier customs processes, and a range of other support for businesses to trade with the world.

####Case studies: UK Export Finance support for UK exporters

England and Scotland: Renewable energy projects in Taiwan

UK Export Finance (UKEF) has provided a £200 million buyer credit guarantee to help finance the Greater Changhua 1 Offshore Wind Farm in Taiwan, unlocking the export potential of the UK’s offshore wind sector. Two renewable energy companies, Seajacks and Trelleborg, have already capitalised on UKEF’s support by winning multi-million-pound export contracts with Ørsted in Taiwan. Seajacks, an East Anglian-based company, will ship the material needed to install the turbines and Trelleborg’s applied technologies operation in the West Midlands will provide protection systems for the cables which connect the turbines to the mainland. UKEF has now provided £500 million of financing for three offshore wind projects in Taiwan alone since late 2019.

####Midlands: Bombardier Transportation trains for new Cairo monorail

UKEF provided a £1.7 billion guarantee to the Egyptian Government to unlock a deal for Bombardier Transportation to supply UK-built trains to a new monorail system in Cairo, the largest amount of financing UKEF has ever provided for an overseas infrastructure project. UKEF support for Bombardier Transportation, which was recently acquired by Alstom, will directly support a new production line in Derby and 100 UK jobs, as well as many more in the UK supply chain. The monorails are a part of Egypt’s plans to build a sustainable transportation infrastructure that can cope with its growing population while reducing emissions.

###Addressing risks and unfair competition

Support for an open and resilient international order also means acting robustly to address unfair competition and those that undermine the rules. We will be proactive to address risks to our national security and economic resilience, and the international order, ensuring our open approach supports our strategic priorities:

- delivering a new National Security and Investment Bill which will give the UK Government decisive powers to intervene in transactions to protect against national security risks whilst increasing certainty for investors; 

- assurance of critical supply chains to ensure our sources of supply are sufficiently diverse and resilient; 

- establishing the Trade Remedies Authority (TRA) as an independent body, enabling businesses to seek redress against unfair trading practices, such as dumping and subsidisation, and surges of imports. This will support stability and predictability for exporters and investors alike;

- using our independent voice at the WTO to highlight and tackle priority risks, including launching trade disputes where necessary

####Our plan for growth - Global Britain

<table class="financial-data source-tableeditor">
<thead>
<tr>
<th>Why this is important</th>
<th>What this means</th>
<th>What we&rsquo;re doing</th>
</tr>
</thead>
<tbody>
<tr>
<td>Working in cooperation with partners can inspire and shape international action on our domestic priorities.</td>
<td>Global action on issues important for the UK to build back better such as climate change and health resilience.</td>
<td>Taking a leading role in international discussions and placing UK priorities at the heart of the multilateral agenda, including through our G7 Presidency and COP26 in Glasgow.<br /><br />Supporting an open and fair trading system including building consensus on WTO reform.<br /><br />Initiatives to enhance competition and procurement in the defence industry.</td>
</tr>
<tr>
<td>Free trade provides UK consumers, businesses and investors with access to cheaper, better quality goods and services.</td>
<td>Creates greater choice and competition, improving productivity, creating jobs and freeing up resources for innovation and investment at home.</td>
<td>Transitioned trade agreements with 64 countries, securing existing relationships.<br /><br />Pipeline of four new Free Trade Agreements (FTAs) to reach 80% of UK trade coverage aim by end 2022. <br /><br />A tailored UK tariff policy.<br /><br />Engage bilaterally to remove market access barriers overseas.</td>
</tr>
<tr>
<td>Openness to inward investment enables the UK to benefit from international capital and knowledge.</td>
<td>Improved productivity and the creation of jobs across the UK.</td>
<td>Ensuring the UK remains a leading destination for global investment by reaffirming the UK Government&rsquo;s commitment to maintaining the strengths that make the UK an attractive location for investment.<br /><br />New Office for Investment to land high-value opportunities aligned with key government priorities, including high-growth sectors, net zero, infrastructure and R&amp;D.<br /><br />New National Security and Investment Bill to provide certainty for investors while ensuring mitigation of security risk.</td>
</tr>
<tr>
<td>UK businesses able to seize the opportunities to sell goods and services around the globe.</td>
<td>More productive businesses that are able to compete and succeed internationally, supporting jobs and UK economic growth.</td>
<td>Developing a new export strategy to align our support for exporters with our plan for growth and sectoral priorities.<br /><br />Opening UK Government trade hubs in Scotland, Wales and Northern Ireland.<br /><br />Increasing UK Export Finance lending capacity and introducing new products.<br /><br />Deepening bilateral relationships outside FTAs: e.g. the Swiss Financial Services mutual recognition agreement.<br /><br />Internationalisation Fund to support exporters. <br /><br />Promoting the UK via the GREAT Britain and Northern Ireland campaign.</td>
</tr>
</tbody>
</table>


