##The way ahead

This plan has set out our ambitious objectives to capitalise on the UK’s dynamic and open economy, tackle long-term problems and drive new growth that will enable us to achieve the people’s priorities.

We must ensure our plan is carried through with a relentless focus on delivery. To drive this, the Prime Minister and Cabinet Secretary have asked Sir Michael Barber to conduct a rapid review of government delivery to ensure that it remains focused, effective and efficient, and to suggest how it could be strengthened.  

This review builds on reforms to the government’s planning and performance framework already underway that aim to embed a new focus on the delivery of outcomes. As part of implementing the Public Value Framework (PVF)<a href="#endnote-001">1</a>, the UK Government published provisional priority outcomes and metrics for UK government departments alongside SR20<a href="#endnote-002">2</a>. These outcomes capture the government’s long-term policy objectives, including the people’s priorities of levelling-up, net zero and global Britain. This plan details how the UK Government will deliver them through economic growth.

These outcomes are now being embedded by departments into Outcome Delivery Plans, which will set out strategy and planning information for delivering the priority outcomes and for delivering on strategic “enabling” activities that are crucial to successful delivery. A new focused performance reporting system at the centre will provide an ongoing picture of departmental activity, with regular reporting to the Cabinet Office and HM Treasury on progress enabling greater shared understanding of performance and early action where delivery is off track. This information will support decisions made at future Spending Reviews, creating a greater link between financial allocations and the delivery of real-world outcomes that matter to citizens.

The Prime Minister has also refreshed the Cabinet Committee structure to reflect the Government’s priorities and to drive the delivery of the plan for growth. The Prime Minister will chair the new National Economy and Recovery Taskforce with a focus on catalysing growth, levelling up across the UK and driving public service performance and delivery.  A new National Economy and Recovery Taskforce (Public Services) committee chaired by the Chancellor of the Duchy of Lancaster will focus on public sector recovery and reform, and the Better Regulation Committee (BRC) chaired by the Chancellor of the Exchequer will coordinate an ambitious programme of regulatory reform over the parliament.  

We will take action over the coming months, building on this strong foundation to make real progress. Further details will be set out in forthcoming publications, including:

<table class="financial-data source-tableeditor">
<tbody>
<tr>
<th><strong>In the next three months&hellip;</strong></th>
<td>Getting Smarter: Knowledge Assets Implementation Strategy<br /><br />Heat and Buildings Strategy<br /><br />Industrial Decarbonisation Strategy<br /><br />Integrated Rail Plan for the Midlands and the North<br /><br />Integrated Review of Security, Defence, Development and Foreign Policy<br /><br />National Bus Strategy</td>
</tr>
<tr>
<th><strong>In the next six months&hellip;</strong></th>
<td>Hydrogen Strategy<br /><br />Innovation Strategy<br /><br />Lifelong Loan Entitlement Consultation<br /><br />National Space Strategy<br /><br />Research &amp; Development Places Strategy<br /><br />Research &amp; Development People and Culture Strategy<br /><br />Transport Decarbonisation Plan</td>
</tr>
<tr>
<th><strong>In the next twelve months&hellip;</strong></th>
<td>Devolution and Local Recovery White Paper<br /><br />Digital Strategy<br /><br />Export Strategy<br /><br />Full Conclusion of the Post-18 Review of Education and Funding<br /><br />Net Zero Strategy<br /><br />Procurement Reform<br /><br />Sector Visions<br /><br />Union Connectivity Review</td>
</tr>
</tbody>
</table>

By combining new approaches with our well-established strengths, we will deliver growth that benefits the whole of the United Kingdom, creates quality jobs, and ensures that we build back better.

###Annex: Opportunities for growth from EU exit

The UK’s departure from the European Union presents opportunities for us to do things differently and better going forward. We have already acted in a number of areas, for example, establishing the new Points Based System on migration and replacing the Common Agricultural Policy. We can capitalise on new regulatory and policy freedoms, and on the UK’s ability to operate on the international stage, finding new ways to drive growth that will enable us to achieve our priority outcomes.

- Attracting the brightest and best: Taking back control of our borders means we decide who can come to the UK, as demonstrated by the new, global Points Based System. Our fairer, firmer and skills-led migration system, with further targeted reforms to high-skilled visa routes planned this year, will enable companies to attract recognised and high-potential talent from around the world, creating jobs and driving growth in the UK.

- Making the most of regulatory flexibility:  Our newfound control of our laws will free the UK to regulate and support our industries in a way that suits our specific needs and creates a dynamic and competitive economy that is fit for the future. We can stimulate growth, innovation and competition in the UK, whilst attracting new investment, enabling businesses to grow dynamically, and maintaining the high standards that the UK has consistently championed. For example, our Transforming Public Procurement green paper shows how we will speed up and simplify our processes and unleash opportunities for small businesses. We will regulate technologies in an innovation-friendly way, driving growth by boosting competition, keeping people safe and secure, and promoting our democratic values. 

- Building our strongest sectors: Ending the Transition Period gives us the ability to supercharge the sectors where we already have competitive advantage, driving economic growth, unleashing investment, creating jobs, and building on our international competitiveness in sectors such as financial services and fintech, automotive, digital, green energy and the creative industries. For example, in Life Sciences, the UK can offer unique opportunities to global companies: we are one of the world’s best research and science bases, underpinned by top class universities, globally renowned clinical research and a uniquely cradle to grave healthcare system in the NHS. New freedoms from being outside of the EU will allow the Medicines and Healthcare products Regulatory Agency (MHRA) to become a globally leading innovative, regulator, acting as an enabler for innovation – as demonstrated by the swift work in approving COVID-19 vaccines.

- Driving growth across the whole UK: We are replacing the overly bureaucratic EU Structural Funds with a new UK Shared Prosperity Fund that will be used to bind together the whole of the United Kingdom, tackling poor outcomes and spreading opportunity in each of the four nations. We are introducing Freeports which will become hives of entrepreneurialism, trade and investment, and we are ensuring that UK companies can trade unhindered in every part of the UK while maintaining world-leading standards for consumers and workers. An agile, futureproofed UK economy with an ambitious global agenda will enable us to level up towns, regions and nations across the whole of the United Kingdom, boosting jobs and ensuring every place shares in the prosperity of the UK.

- Supercharging green growth: The UK will also set its own climate and energy policies, based on our ambitious domestic and international climate objectives. We want to break away from the pack with ambitious targets and faster progress. The landmark Environment Bill will tackle the biggest environmental priorities of our time and change the way we protect and enhance our precious natural resources in England. The Bill will require the government to set long-term legally binding environmental targets, establishing a new independent public body, the Office for Environmental Protection (OEP) to hold the UK Government to account. The Environment Bill will also introduce a number of measures for England further improving our environment, from mandatory Biodiversity Net Gain and Local Nature Recovery Strategies, to the power to introduce a Deposit Return Scheme and increase our resource efficiency. We are actively exploring opportunities to cut emissions and boost innovation in the aviation sector, including through the Jet Zero Council and the CAA consultation on opportunities for UK General Aviation. We are replacing the Common Agricultural Policy with a system in England that will enable better environmental outcomes. By taking back control of our territorial waters, we will be able to manage our fisheries and precious marine environment in a more sustainable way.

- A world leader in free trade: Internationally, the UK will operate an independent trade policy for the first time in 50 years. We have introduced the UK Global Tariff tailored to the needs of the UK economy, almost doubling the number of goods that are tariff free relative to the EU Common External Tariff, and supporting businesses and their supply chains by making it easier and cheaper to import goods from overseas. We will use our independent voice at the World Trade Organization to champion free and fair trade, the rules based international system, and promote openness and cooperation in the face of collective challenges. The new Free Trade Agreements we strike – like those we are already negotiating with the US, Australia and New Zealand – will increase our trade with the rest of the world, create new opportunities for our exporters and deliver better choice and value for our consumers.  

!!1




